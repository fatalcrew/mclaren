import React, {Component} from "react";

export default class DisplayImage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="display-image">
                <div className={this.props.imageSize && "display"}>
                    <img height="100%" width="100%" src={this.props.displayImage}/>
                </div>
            </div>
        );
    }
}