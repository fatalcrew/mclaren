import React from "react";
import FontIcon from 'material-ui/FontIcon';
import GridListOneLine from "../_/component/GridListOneLine";
import PurchaseTab from "../_/component/PurchaseTab";
import DisplayImage from "./DisplayImage";
import ScrollableAnchor, {goToTop} from 'react-scrollable-anchor';
import update from "react-addons-update";

export default class Product extends React.Component {
    constructor(props) {
        super(props);
        this.id = props.params.id;
        this.state = {
            product: {
                "images": [
                    {
                        showDetail: "https://s-media-cache-ak0.pinimg.com/236x/1c/5d/8c/1c5d8c8e4d907d778b21957defc90b10.jpg",
                        showDisplay: "https://s-media-cache-ak0.pinimg.com/236x/1e/c1/6c/1ec16ca3fa156616074f7f24076907b4.jpg",
                        title: "alpha",
                        id: 0,
                    },
                    {
                        showDetail: "https://s-media-cache-ak0.pinimg.com/236x/90/04/d2/9004d2a46991761bd443f2cddd13b557.jpg",
                        showDisplay: "https://s-media-cache-ak0.pinimg.com/236x/d0/32/15/d03215745e0c3447e63adaefe8a83df1.jpg",
                        title: "beta",
                        id: 1,
                    },
                    {
                        showDetail: "https://s-media-cache-ak0.pinimg.com/236x/84/1f/6d/841f6dd5a8bb32f860b2ee15858052ba.jpg",
                        showDisplay: "https://s-media-cache-ak0.pinimg.com/236x/e1/b0/79/e1b079b27024c6ea5c734d4a6064c20b.jpg",
                        title: "delta",
                        id: 2,
                    },
                    {
                        showDetail: "https://s-media-cache-ak0.pinimg.com/236x/df/16/3e/df163e05544e37dc9d64f6af10c631d4.jpg",
                        showDisplay: "https://s-media-cache-ak0.pinimg.com/236x/23/9d/6b/239d6ba2c0ec55829767314487f86d54.jpg",
                        title: "zeta",
                        id: 3,
                    },
                ],
                "name": "수납보드",
                "id": "0"
            },
            viewImage: "https://s-media-cache-ak0.pinimg.com/236x/1e/c1/6c/1ec16ca3fa156616074f7f24076907b4.jpg",
            detailImage: "https://s-media-cache-ak0.pinimg.com/236x/1c/5d/8c/1c5d8c8e4d907d778b21957defc90b10.jpg",
            toggle: true,
        };
        this.anchorTop = this.anchorTop.bind(this);
    };

    anchorTop(product) {
        this.setState(
            update(
                this.state,
                {
                    viewImage: {$set: product.showDisplay},
                    detailImage: {$set: product.showDetail},
                }
            )
        );
        goToTop();
    };

    render() {
        return (
            <div className="product">
                <DisplayImage imageSize="300px" displayImage={this.state.viewImage}/>
                <GridListOneLine className="container" anchorTop={this.anchorTop} product={true}
                                 slideData={this.state.product}/>
                <div className="description-view">
                    <div className="row">
                        <div className="title col-xs-8">
                            <span className="title">창조의아침</span>
                        </div>
                        <div className="col-xs-4 price text-right">
                        <span className="price">
                            <span>$400</span>
                        </span>
                        </div>
                    </div>

                    <div className="instruction">
                        <div className="description">
                            가장뛰어난 예언자는 과거이다
                            바이런
                        </div>
                        <PurchaseTab data={this.state.product}/>
                        <DisplayImage displayImage={this.state.detailImage}/>
                    </div>
                    <PurchaseTab data={this.state.product}/>
                    <hr />
                    <div className="relation-product">
                        연관상품
                    </div>
                </div>

            </div>
        );
    }
}