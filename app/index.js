import React from 'react';
import ReactDOM from 'react-dom';
import Router from './Router';
import "./_/css/bundle.less";

// import "slick-carousel"
const rootElement = document.getElementById('home-body');
ReactDOM.render(<Router />, rootElement);

