import React, {Component} from 'react';
import ReactSummernote from 'react-summernote';
import 'react-summernote/dist/react-summernote.css'; // import styles
import 'react-summernote/lang/summernote-ko-KR'; // you can import any other locale
import {Button} from 'antd';
import axios from 'axios';

import 'bootstrap/js/modal';
import 'bootstrap/js/dropdown';
import 'bootstrap/js/tooltip';
import 'bootstrap/dist/css/bootstrap.css';

export default class ReviewEdit extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.insertImage = this.insertImage.bind(this);
    }

    onChange(content) {
        console.log('onChange', content);
    }

    insertImage(url, filenameOrCallback) {
        console.log(url);
        console.log(JSON.parse(localStorage.getItem('me')));
        console.log(JSON.parse(localStorage.getItem('me')).token);
        axios
            .post(url, {file : filenameOrCallback[0]},
                {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': JSON.parse(localStorage.getItem('me')).token
                    },
                })
            .then((res) => {
                console.log(res);
            });
        console.log(filenameOrCallback);
    };

    insertText(text) {

    }

    render() {
        return (
            <div className="review-edit">
                <input className="form-control title-field" placeholder="제목을 입력하세요."/>
                <ReactSummernote
                    className="review-summer-note"
                    options={{
                        lang: 'ko-KR',
                        height: 400,
                        dialogsInBody: false,
                        toolbar: [
                            ['style', ['style']],
                            ['para', ['paragraph']],
                            ['font', ['bold', 'underline', 'clear']],
                            ['insert', ['link', 'picture']],
                        ],
                    }}
                    onChange={this.onChange}
                    onImageUpload={this.insertImage.bind(this, process.env.APP_HOST + "/api/attach-file")}
                />
                <Button className="submit-btn clearfix" type="primary">등록</Button>
            </div>

        );
    }
}