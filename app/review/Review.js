import React, {Component} from "react";
import update from 'react-addons-update';
import axios from 'axios';
import Comments from '../comments/Comments';

export default class Review extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reviewData: {}
        };
        this.id = this.props.params.id;
    }

    componentWillMount() {
        axios
            .get(process.env.APP_HOST + '/api/review/' + this.id)
            .then((res) => {
                this.setState(
                    update(
                        this.state,
                        {
                            reviewData: {$set: res.data.data.items}
                        }
                    )
                )
            })
            .catch((error) => {
            });
    }

    render() {
        return (
            <div className="review">
                <div>
                    <div>
                        <h3 className="text-center">
                            {this.state.reviewData.title}
                        </h3>
                        <div className="text-right">
                            <div className="col-md-offset-4 col-md-4">
                                작성일 : {this.state.reviewData.created_at}
                            </div>
                            <div className="col-md-4">
                                by {this.state.reviewData.user_nick}
                            </div>
                        </div>
                    </div>
                    <div dangerouslySetInnerHTML={ {__html: this.state.reviewData.contents}}>
                    </div>
                    <Comments commentId={this.id}/>
                </div>
            </div>
        )
    }
}
