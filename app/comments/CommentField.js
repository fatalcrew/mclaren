import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import update from 'react-addons-update';
import FlatButton from 'material-ui/FlatButton';

export default class CommentField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingToggle: false,
        };
        this.insertComment = this.insertComment.bind(this);
    }

    insertComment() {
        this.setState(
            update(
                this.state,
                {
                    loadingToggle: {$set: true},
                }
            ));

    }

    render() {
        return (
            <div className="comment-field">
                <TextField
                    style={{
                        fontSize: "13px",
                        position: "absolute",
                        left: "0",
                        marginLeft: "5px",
                        width: "79%",
                        marginRight: "53px"
                    }}
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                    hintText="댓글을 입력해주세요."
                    multiLine={true}
                    rows={2}
                    rowsMax={2}
                />
                <FlatButton
                    style={{
                        fontSize: "13px",
                        position: "absolute",
                        marginRight: "5px",
                        right: "0",
                        minWidth: "48px",
                        height: "48px",
                        border: "1px solid black",
                        marginTop: "16px"
                    }}
                    onTouchTap={this.insertComment}
                    disabled={this.state.loadingToggle}
                    label={this.state.loadingToggle == false && "작성" || <img src="../_/img/loding.svg"/>}
                />
            </div>
        )
    }
}