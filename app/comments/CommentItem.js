import React, {Component} from 'react';
import {ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import {grey400} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

const iconButtonElement = (
    <IconButton
        touch={true}
        tooltip="more"
        tooltipPosition="bottom-left"
    >
        <MoreVertIcon color={grey400}/>
    </IconButton>
);

const rightIconMenu = (
    <IconMenu
        iconButtonElement={iconButtonElement}
        style={{
            marginTop: "-7px"
        }}
    >
        <MenuItem
            innerDivStyle={{
                fontSize: "13px"
            }}
        >
            답글
        </MenuItem>
        <MenuItem
            innerDivStyle={{
                fontSize: "13px"
            }}
        >
            삭제
        </MenuItem>
        <MenuItem
            innerDivStyle={{
                fontSize: "13px"
            }}
        >
            수정
        </MenuItem>
    </IconMenu>
);


export default class CommentItem extends Component {

    render() {
        return (
            <div>
                <div className={this.props.comments || 'hide'}>
                    <ListItem
                        innerDivStyle={{
                            marginRight: "10px",
                            marginTop: "10px",
                            fontSize: "14px",
                            textAlign: "left",
                            paddingTop: "10px",
                            paddingRight: "40px",
                            paddingBottom: "10px",
                            minHeight: "55px"
                        }}
                        leftAvatar={<Avatar src="http://www.material-ui.com/images/ok-128.jpg"/>}
                        primaryText={
                            <div>
                                {
                                    this.props.comments
                                }
                            </div>
                        }
                        className="review-right-icon"
                        rightIcon={rightIconMenu}
                    />
                    <Divider inset={true}/>
                </div>
                <div className={!this.props.comments || 'hide'}>
                    댓글이 없습니다.
                </div>
            </div>
        )
    }
}