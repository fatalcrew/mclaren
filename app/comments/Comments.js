import React, {Component} from "react";
import CommentItem from './CommentItem';
import CommentField from './CommentField';
import update from 'react-addons-update';
import axios from 'axios';
import Subheader from 'material-ui/Subheader';
import {List} from 'material-ui/List';

export default class Comments extends Component {

    constructor(props) {
        super(props);
        this.state = {
            commentItems: [{
                comments: "",
                show: false,
            }]
        };
        this.getCommentList = this.getCommentList.bind(this);
    }

    componentWillMount() {
        this.getCommentList();
    }

    getCommentList() {
        axios.get(process.env.APP_HOST + '/api/comments/' + this.props.commentId)
            .then((res) => {
                this.setState(
                    update(
                        this.state,
                        {
                            commentItems: {$set: res.data.data.items}
                        }
                    )
                );
            })
            .catch((error) => {
            })
    }

    render() {
        return (
            <div className="text-center">
                <List>
                    <Subheader>댓글</Subheader>
                    {this.state.commentItems.map((v, k) => {
                        return (
                            <CommentItem key={k} comments={v.comments}/>
                        )
                    })}
                </List>
                <CommentField reRenderComment={this.getCommentList} />
            </div>
        )
    }
}