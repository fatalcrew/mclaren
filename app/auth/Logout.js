import React from 'react';
import {browserHistory} from 'react-router';

export default class Logout extends React.Component {
    constructor(props) {
        super(props);
        localStorage.removeItem('me');
        browserHistory.push("/");
    }

    render() {
        return (
            <span>
                Please wait...
            </span>
        )
    }
}
