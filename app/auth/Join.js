import React, {Component, findDOMNode} from "react";
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import axios from 'axios';
import update from 'react-addons-update';

export default class Join extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            pass: "",
            passChk: "",
        };
        this.setName = this.setName.bind(this);
        this.setPass = this.setPass.bind(this);
        this.setPassChk = this.setPassChk.bind(this);
        this.setEmail = this.setEmail.bind(this);
        this.passChkForRegister = this.passChkForRegister.bind(this);
    }

    passChkForRegister() {
        if(this.state.pass != this.state.passChk){
            alert('비밀번호가 틀립니다.');
            return false;
        }
        axios.post(process.env.APP_HOST+'/api/auth/register', this.state)
            .then(() => {
                location.href='/welcome';
            })
            .catch(response => {

            });
    }

    setName(value) {
        this.setState(
            update(
                this.state,
                {
                    name: {$set: value},
                }
            )
        );
    }

    setPass(value) {
        this.setState(
            update(
                this.state,
                {
                    pass: {$set: value},
                }
            )
        );
    }

    setPassChk(value) {
        this.setState(
            update(
                this.state,
                {
                    passChk: {$set: value},
                }
            )
        );
    }

    setEmail(value) {
        this.setState(
            update(
                this.state,
                {
                    email: {$set: value},
                }
            )
        )
    }

    render() {
        return (
            <div className="join">
                <TextField
                    className="field offset-1"
                    floatingLabelText="Name"
                    hintText="이름"
                    onChange={(k, v) => {
                        this.setName(v);
                    }}
                    ref='name'
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                />
                <TextField
                    className="field offset-1"
                    type="email"
                    floatingLabelText="Email"
                    hintText="이메일"
                    onChange={(k, v) => {
                        this.setEmail(v);
                    }}
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                />
                <TextField
                    className="field offset-1"
                    floatingLabelText="Password"
                    hintText="비밀번호"
                    type="password"
                    onChange={(k, v) => {
                        this.setPass(v);
                    }}
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                />
                <TextField
                    className="field offset-1"
                    floatingLabelText="Confirm Password"
                    hintText="비밀번호 확인"
                    type="password"
                    onChange={(k, v) => {
                        this.setPassChk(v);
                    }}
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                />
                <FlatButton
                    className="button join-button  offset-1"
                    label="가입하기"
                    hoverColor="white"
                    onTouchTap={this.passChkForRegister}
                    style={{
                        lineHeight: "0",
                    }}
                    labelStyle={{
                        lineHeight: "0",
                        fontWeight: "200",
                    }}
                />
            </div>
        )
    }
}