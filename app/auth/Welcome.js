import React, {Component} from "react";
import FlatButton from 'material-ui/FlatButton';
import Route, {Link, browserHistory} from "react-router";

export default class Welcome extends Component {
    constructor(props) {
        super(props);

    }

    redirectUrl(url) {
        location.href = url;
    }

    render() {
        return (
            <div className="text-center">

                <div className="row">
                    <h1>
                        환영합니다.
                    </h1>
                </div>

                <FlatButton
                    className="button"
                    label="둘러보기"
                    hoverColor="white"
                    onTouchTap={
                        this.redirectUrl.bind(this, "/")
                    }
                    labelStyle={{
                        fontWeight: "200",
                    }}
                />
                <FlatButton
                    className="button"
                    label="로그인"
                    hoverColor="white"
                    onTouchTap={
                        this.redirectUrl.bind(this, "/login")
                    }
                    labelStyle={{
                        fontWeight: "200",
                    }}
                />

            </div>
        )
    }
}