import React, {Component} from "react";
import ReactDom from "react-dom";
import Router, {Link, browserHistory} from 'react-router';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import axios from 'axios';
import update from 'react-addons-update';
import { message } from 'antd';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            pass: "",
            errorText: "",
            errorStyle: "",
        };
        this.setUserName = this.setUserName.bind(this);
        this.setPass = this.setPass.bind(this);
        this.setError = this.setError.bind(this);
    }

    requireAuth(res) {
        location.href = res;
    }

    setUserName(value) {
        this.setState(
            update(
                this.state,
                {
                    username: {$set: value},
                }
            )
        );
    }

    setPass(value) {
        this.setState(
            update(
                this.state,
                {
                    pass: {$set: value},
                }
            )
        );
    }

    setError(message) {
        this.setState(
            update(
                this.state,
                {
                    errorText: {$set: message},
                    errorStyle: {$set: message && " "}
                }
            )
        )
    }

    getToken() {
        this.setError("");
        axios.post(process.env.APP_HOST+'/api/auth/user-check', this.state)
            .then(response => {
                message.success('로그인 성공');
                localStorage.setItem('me', JSON.stringify(response.data.data.items));
                browserHistory.push("/mypage");
            })
            .catch((e) => {
                message.error(e.response.data.data.message);
            });
    }

    render() {
        return (
            <div className="login">
                <TextField
                    className="log-id offset-1"
                    floatingLabelText="ID"
                    type="email"
                    errorText={this.state.errorStyle}
                    onChange={(k, v) => {
                        this.setUserName(v);
                    }}
                    hintText="아이디를 입력하세요."
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                />
                <TextField
                    className="log-id offset-1"
                    floatingLabelText="PASS"
                    hintText="비밀번호를 입력하세요."
                    errorText={this.state.errorText}
                    onChange={(k, v) => {
                        this.setPass(v)
                    }}
                    type="password"
                    underlineFocusStyle={{
                        borderBottomColor: "black"
                    }}
                    floatingLabelFocusStyle={{
                        color: "black",
                    }}
                />
                <FlatButton
                    className="button login-button offset-1"
                    label="들어가기"
                    hoverColor="white"
                    onTouchTap={this.getToken.bind(this)}
                    style={{
                        lineHeight: "0",
                    }}
                    labelStyle={{
                        lineHeight: "0",
                        fontWeight: "200",
                    }}
                />

                <FlatButton
                    className="button another-button offset-1"
                    label="회원가입"
                    hoverColor="black"
                    backgroundColor="black"
                    onTouchTap={this.requireAuth.bind(this, '/join')}
                    rippleColor="gray"
                    style={{
                        lineHeight: "0",
                    }}
                    labelStyle={{
                        lineHeight: "0",
                        color: "white",
                        fontWeight: "200",
                    }}
                />

                <FlatButton
                    className="button another-button"
                    label="비밀번호찾기"
                    hoverColor="white"
                    style={{
                        lineHeight: "0",
                    }}
                    labelStyle={{
                        lineHeight: "0",
                        fontWeight: "200",
                    }}
                />
            </div>
        );
    }
}