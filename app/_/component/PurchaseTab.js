import React, {Component} from "react";
import FontIcon from 'material-ui/FontIcon';
import Modal from "react-modal";
import Dialog from 'material-ui/Dialog';
import SelectField from "./SelectField";
import update from "react-addons-update";
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        width: "100%",
        transform: 'translate(-50%, -50%)'
    }
};


export default class Purchase extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            selectedData: [],
        };
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.insertSelectedData = this.insertSelectedData.bind(this);
    }

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    insertSelectedData(selectedData) {
        let selected = this.state.selectedData.concat([selectedData]);
        this.setState(
            update(
                this.state, {
                    selectedData: {
                        $set: selected
                    }
                }
            )
        );
    }

    render() {
        return (
            <div>
                <Dialog
                    title={<SelectField data={this.props.data} insertSelectedData={this.insertSelectedData}/>}
                    actions={[
                        <FlatButton
                        label="Cancel"
                        primary={true}
                        onTouchTap={this.handleClose}
                        />,
                        <FlatButton
                        label="Submit"
                        primary={true}
                        disabled={true}
                        onTouchTap={this.handleClose}
                        />
                    ]}
                    modal={true}
                    contentStyle={{
                        width: "100%",
                        maxWidth: 'none',
                    }}
                    titleClassName={
                        "text-center"
                    }
                    titleStyle={{
                        margin: "0 10px",
                        width:"80%",
                    }}
                    bodyStyle={{
                        padding: "0 5px 5px",
                        overflowY:"auto",
                    }}
                    open={this.state.open}
                >
                    <table className="table">
                        <div className="selected-field">
                            {this.state.selectedData.map((v, k) => {
                                return (
                                    <tr>
                                        <td className="col-xs-2">
                                            <img width="100%" src={v.showDisplay}/>
                                        </td>
                                        <td className="col-xs-3">
                                            {v.title}
                                        </td>
                                        <td className="col-xs-3">
                                            1개
                                        </td>
                                        <td className="col-xs-4">
                                            $400
                                        </td>
                                    </tr>
                                )
                            })}
                        </div>
                    </table>
                </Dialog>
                <div className="purchase-tab">
                    <div className="action-items text-center row">
                        <FlatButton className="col-xs-4" onTouchTap={this.handleOpen}
                                    labelStyle={{color: "white"}} style={{borderRadius: "0px"}}
                                    backgroundColor={"black"} label={"구매하기"}/>
                        <FlatButton className="col-xs-4" style={{borderRadius: "0px"}} backgroundColor={"#F9F9F9"}
                                    label={"리뷰" + "30"}/>
                        <FlatButton className="col-xs-4" style={{borderRadius: "0px"}} backgroundColor={"#F9F9F9"}
                                    label={"담기"}/>
                    </div>
                </div>
            </div>
        );
    }
}