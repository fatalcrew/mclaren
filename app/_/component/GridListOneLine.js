import React, {Component} from "react";
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
        display: 'flex',
        flexWrap: 'nowrap',
        overflowX: 'auto',
        overflowY: 'none',
    },
};

export default class GridListOneLine extends Component {
    render() {
        return (
            <div className="grid-list-one-line" style={styles.root}>
                <GridList style={styles.gridList} cols={2.2} cellHeight={120}>
                    {this.props.slideData.images.map((image, k) => (
                        <GridTile
                            key={k}
                            style={{
                                height:"100px"
                            }}
                        >
                            <button className="action-anchor-top" onClick={this.props.product && this.props.anchorTop.bind(this, image)}>
                                <img className="show-display" src={image.showDisplay}/>
                            </button>
                        </GridTile>
                    ))}
                </GridList>
            </div>
        );
    }
}