import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

export default class Select extends Component {
    state = {
        values: "",
    };

    selectionRenderer = (values) => {
        switch (values.length) {
            case 0:
                return '';
            default:
                return persons[this.state.values].title;
        }
    };

    menuItems(purchaseData) {
        return purchaseData.images.map((person, k) => (
            <MenuItem
                key={person.id}
                onTouchTap={this.props.insertSelectedData.bind(this, person)}
                value={person.title}
                primaryText={person.title}
            />
        ));
    }

    render() {
        return (
            <SelectField
                multiple={false}
                style={{
                    margin:"auto",
                    display:"block",
                    width:"80%"
                }}
                selectedMenuItemStyle={{
                    width:"100%",
                }}
                underlineStyle={"none"}
                hintText="선택하세요."
                value={this.state.values}
                onChange={this.insertSelectedData}
                selectionRenderer={this.selectionRenderer}
            >
                {this.menuItems(this.props.data)}
            </SelectField>
        );
    }
}