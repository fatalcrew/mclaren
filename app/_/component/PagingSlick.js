import React, { Component } from 'react'
import Slider from 'react-slick'

let baseUrl = "https://s3.amazonaws.com/static.neostack.com/img/react-slick";

export default class PagingSlick extends Component {
    render() {
        const settings = {
            customPaging: function(i) {
                return <a><img width="100%" src={`${baseUrl}/abstract0${i+1}.jpg`}/></a>
            },
            dots: true,
            dotsClass: 'slick-dots slick-thumb',
            infinite: false,
            speed: 500,
            arrows:false,
            slidesToShow: 1,
        };
        return (
            <div>
                <Slider {...settings}>
                    <div>
                        <img width="100%" src={baseUrl + '/abstract01.jpg'} />
                    </div>
                    <div>
                        <img width="100%" src={baseUrl + '/abstract02.jpg'} />
                    </div>
                    <div>
                        <img width="100%" src={baseUrl + '/abstract03.jpg'} />
                    </div>
                    <div>
                        <img width="100%" src={baseUrl + '/abstract04.jpg'} />
                    </div>
                </Slider>
            </div>
        )
    }
}