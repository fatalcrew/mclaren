import React, {Component, PropTypes} from 'react';
import CommonHeader from "./layout/CommonHeader";
import CommonFooter from "./layout/CommonFooter";
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toggle : true
        };
    }

    render() {
        return (
            <div>
                <CommonHeader toggle={this.state.toggle}/>
                <div className="detail">
                    {this.props.children}
                </div>
                <CommonFooter path={this.props.location.pathname}/>
            </div>
        );
    }
}
App.propTypes = {
    children: PropTypes.element.isRequired,
};
export default App;