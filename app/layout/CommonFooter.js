import React from "react";
import {Link, browserHistory, Router, IndexLink} from "react-router";
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import Paper from 'material-ui/Paper';
import * as _ from "lodash";
import Drawer from 'material-ui/Drawer';
import RaisedButton from 'material-ui/RaisedButton';
import Category from '../category/Category';

export default class CommonFooter extends React.Component {
    constructor(props) {
        super(props);
        this.path = _.split(props.path, "/")[1];
        this.state = {open: false};
    }

    historyBack = () => {
        browserHistory.goBack();
    };


    handleToggle = () => this.setState({open: !this.state.open});

    handleClose = () => this.setState({open: false});

    render() {
        return (
            <div>
                <div className={"back-tap"}>
                    <a onClick={this.historyBack.bind()}>
                        <FontIcon className="material-icons footer-item">chevron_left</FontIcon>
                    </a>
                </div>
                <Drawer
                    docked={false}
                    width={200}
                    open={this.state.open}
                    disableSwipeToOpen={false}
                    onRequestChange={(open) => this.setState({open})}
                >
                    <Category/>
                </Drawer>
                <Paper className="common-footer text-center" zDepth={1}>

                    <BottomNavigation>
                        <RaisedButton
                            style={{
                                boxShadow:"none",
                            }}
                            icon={<FontIcon className="material-icons footer-item">dehaze</FontIcon>}
                            onTouchTap={this.handleToggle}
                        />
                        <Link to="/">
                            <FontIcon className="material-icons footer-item">home</FontIcon>
                        </Link>
                        <Link to="/favorite">
                            <FontIcon className="material-icons footer-item">favorite</FontIcon>
                        </Link>
                        <Link to="/mypage">
                            <FontIcon className="material-icons footer-item">person</FontIcon>
                        </Link>
                    </BottomNavigation>
                </Paper>
            </div>
        );
    }
}