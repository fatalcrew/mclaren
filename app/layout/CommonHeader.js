import React from "react";
import HeadRoom from "react-headroom";
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import update from "react-addons-update";

export default class CommonHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            show: true
        };
        this.showSearch = this.showSearch.bind(this);
    }

    showSearch(flag) {
        this.setState(
            update(
                this.state,
                {
                    show: {$set: flag}
                }
            )
        );
        if (!flag) {
            this.nameInput.focus();
        }

    }

    render() {
        return (
            <HeadRoom>
                <div className="common-header">
                    <div className="">
                        <div className="text-center logo col-xs-offset-2 col-xs-8">
                            <span>In. Gallery</span>
                        </div>
                        <form
                            className={`mobile-search-form ${this.state.show ? "enter enter-active" : "leave leave-active"}`}>
                            <input
                                ref={(input) => {
                                    this.nameInput = input;
                                }}
                                onBlur={this.showSearch.bind(this, true)}
                                className="search-text col-xs-10"
                                type="text" placeholder="검색어를 입력하세요."
                            />
                            <button type="button" className="col-xs-2 mobile-search-btn">
                                <FontIcon className="material-icons search-icon">search</FontIcon>
                            </button>
                        </form>
                        <button type="button" className="col-xs-2 text-right"
                                onClick={this.showSearch.bind(this, false)}>
                            <FontIcon className="material-icons search-icon">search</FontIcon>
                        </button>
                    </div>
                </div>
            </HeadRoom>
        );
    }
}