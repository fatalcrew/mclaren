import React from "react";
import axios from "axios";
import update from "react-addons-update";
import HomeList from "./HomeList";

export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            reviews: [
                {
                    id : "",
                    cover_img: "",
                }
            ]
        }
    };

    render() {
        return (
            <div className="homeGallery">
                <HomeList reviews={this.state.reviews}/>
            </div>
        );
    };

    componentWillMount() {
        axios.get(process.env.APP_HOST + '/api/review')
            .then((res) => {
                this.setState({reviews: res.data.data.items});
            })
            .catch((err) => {
            });
    }

};