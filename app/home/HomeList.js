import React, {Component} from "react";
import {Link} from "react-router";

export default class HomeList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                {this.props.reviews.map((v) => {
                    return (
                        <div className="home-list" key={v.id}>
                            <Link to={"/review/" + v.id}>
                                <div className="home-list-item text-center">
                                    <img width="100%" src={v.cover_image}/>
                                </div>
                            </Link>
                        </div>
                    )
                })}
            </div>
        )
    }
}