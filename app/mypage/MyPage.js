import React from "react";
import {Timeline, Badge, Button} from 'antd';
import Router, {Link, browserHistory} from "react-router";

export default class MyPage extends React.Component {
    componentWillMount() {
        if (!localStorage.getItem('me')) {
            browserHistory.push('/login');
        }
    }

    render() {
        return (
            <div className="mypage">
                <div className="profile-area text-center clearfix">
                    <div className="profile-image col-xs-offset-1 col-xs-3">
                        <img width="100%" src="https://d3q6qq2zt8nhwv.cloudfront.net/m/1_extra_9bm6rw7n.jpg"/>
                    </div>
                    <div className="col-xs-8 text-left">
                        <div className="nick-name col-xs-6">
                            게스트
                        </div>
                        <div className="col-xs-6 text-right">
                            <Badge count={5}>
                                <i className="material-icons" style={{
                                    verticalAlign: 'super'
                                }}>mail_outline</i>
                            </Badge>
                            <i className="material-icons col-xs-offset-2" style={{
                                verticalAlign: 'text-bottom'
                            }}>more_horiz</i>
                        </div>
                    </div>
                </div>
                <div className="clearfix">
                    <div className="text-center col-xs-6">
                        <h4>내가 작성한 리뷰</h4>
                        <Timeline pending={<Link to="">See more</Link>}>
                            <Timeline.Item>Create a services site 2015-09-01</Timeline.Item>
                            <Timeline.Item>Solve initial network problems 2015-09-01</Timeline.Item>
                            <Timeline.Item>Technical testing 2015-09-01</Timeline.Item>
                            <Timeline.Item>Technical testing 2015-09-01</Timeline.Item>
                        </Timeline>
                    </div>
                    <div className="text-center col-xs-6">
                        <h4>내 활동 기록</h4>
                        <Timeline pending={<Link to="">See more</Link>}>
                            <Timeline.Item>Create a services site 2015-09-01</Timeline.Item>
                            <Timeline.Item>Solve initial network problems 2015-09-01</Timeline.Item>
                            <Timeline.Item>Technical testing 2015-09-01</Timeline.Item>
                            <Timeline.Item>Technical testing 2015-09-01</Timeline.Item>
                        </Timeline>
                    </div>
                </div>
                <div className="clearfix">
                    <Button className="col-xs-offset-1 col-xs-4" type="primary">Feedback 보내기</Button>
                    <Button className="col-xs-offset-2 col-xs-4" type="primary">질문하기</Button>
                </div>
            </div>
        );
    }
}