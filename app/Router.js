import React from 'react';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';
import App from './App';
import Home from './home/Home';
import MyPage from './mypage/MyPage';
import Community from './community/Community';
import Cart from './cart/Cart';
import Login from './auth/Login';
import Join from './auth/Join';
import Logout from './auth/Logout';
import Product from './product/Product';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Favorite from './favorite/Favorite';
import Welcome from "./auth/Welcome";
import Review from "./review/Review";
import ReviewList from "./review/ReviewList";
import ReviewEdit from "./review/ReviewEdit";


export default class RouterAction extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <MuiThemeProvider>
                <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
                    <Route path="/" component={App}>
                        <IndexRoute component={Home}/>
                        <Route path="/mypage" component={MyPage}/>
                        <Route path="/community" component={Community}/>
                        <Route path="/favorite" component={Favorite}/>
                        <Route path="/cart" component={Cart}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/join" component={Join}/>
                        <Route path="/welcome" component={Welcome}/>
                        <Route path="review">
                            <IndexRoute component={ReviewList}/>
                            <Route path="write" component={ReviewEdit}/>
                            <Route path=":id" component={Review}/>
                        </Route>
                        <Route to="help" path="/Product/(:id)" component={Product}/>
                        <Route path="/logout" component={Logout}/>
                    </Route>
                </Router>
            </MuiThemeProvider>
        );
    }
}