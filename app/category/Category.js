import React from "react";
import {List, ListItem} from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import RaisedButton from 'material-ui/RaisedButton';

export default class Category extends React.Component {
    state = {
        open: false,
    };

    style = {};

    handleToggle = () => {
        this.setState({
            open: !this.state.open,
        });
    };

    handleNestedListToggle = (item) => {
        this.setState({
            open: item.state.open,
        });
    };

    render() {
        return (
            <div className="category">
                <div className="text-left title">
                    <h5><b>카테고리</b></h5>
                </div>
                <hr className="hr-division"/>
                <List className="menu-field">
                    <ListItem
                        className="depth-a"
                        primaryText="페인트"
                        primaryTogglesNestedList={true}
                        nestedListStyle={{
                            padding: "0"
                        }}
                        innerDivStyle={{
                            padding: "13px 16px",
                        }}
                        nestedItems={[
                            <ListItem
                                key={1}
                                innerDivStyle={{
                                    padding:"8px 16px",
                                }}
                                className="depth-b"
                                primaryText="페인트 보조재"
                            />,
                            <ListItem
                                key={2}
                                innerDivStyle={{
                                    padding:"8px 16px",
                                }}
                                className="depth-b"
                                primaryText="페인트 도구"
                            />
                        ]}
                    />
                    <ListItem
                        className="depth-a"
                        primaryText="커튼"
                        nestedListStyle={{
                            padding: "0"
                        }}
                        innerDivStyle={{
                            padding: "13px 16px",
                        }}
                    />
                    <ListItem
                        primaryText="조명"
                        className="depth-a"
                        primaryTogglesNestedList={true}
                        nestedListStyle={{
                            padding: "0"
                        }}
                        innerDivStyle={{
                            padding: "13px 16px",
                        }}
                        nestedItems={[
                            <ListItem
                                key={1}
                                className="depth-b"
                                innerDivStyle={{
                                    padding:"8px 16px",
                                }}
                                primaryText="벽등"
                            />,
                            <ListItem
                                key={2}
                                className="depth-b"
                                innerDivStyle={{
                                    padding:"8px 16px",
                                }}
                                primaryText="거실등"
                            />,
                            <ListItem
                                key={3}
                                primaryText="스탠"
                                innerDivStyle={{
                                    padding:"8px 16px",
                                }}
                                className="depth-b"
                                primaryTogglesNestedList={true}
                            />,
                        ]}
                    />
                </List>
                <div className="short-btn">
                    <BottomNavigation style={{
                        background: "rgba(0, 0, 0, 0.1)"
                    }}>
                        <RaisedButton
                            backgroundColor="rgba(0, 0, 0, 0.1)"
                            style={{
                                borderRadius:"none",
                                boxShadow:"none",
                                background: "rgba(0, 0, 0, 0.1)"
                            }}
                            icon={<FontIcon className="material-icons short-item">compare_arrows</FontIcon>}
                        />
                        <RaisedButton
                            backgroundColor="rgba(0, 0, 0, 0.1)"
                            style={{
                                borderRadius:"none",
                                boxShadow:"none",
                                background: "rgba(0, 0, 0, 0.1)"
                            }}
                            icon={<FontIcon className="material-icons short-item">question_answer</FontIcon>}
                        />
                    </BottomNavigation>
                </div>
            </div>
        );
    }
}