import React from 'react';
import {Card} from 'antd';

const arr = [1, 2, 3, 4, 5];

export default class Favorite extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="favorite container row">
                {arr.map((v,k) => {
                    return (
                        <div className="col-xs-6 card" key ={k} >
                            <Card style={{width: '100%'}} bodyStyle={{padding: 0}}>
                                <div className="custom-image">
                                    <img alt="example" width="100%"
                                         src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"/>
                                </div>
                                <div className="custom-card">
                                    <h5>Europe Street beat</h5>
                                    <p>www.instagram.com</p>
                                </div>
                            </Card>
                        </div>
                    )
                })}
                <div className="no-found-list text-center hide">
                    <i className="material-icons">inbox</i>
                    <br/>
                    현재 찜한 목록이 없습니다.
                </div>
            </div>
        );
    }
}