const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
var _ = require("lodash");
var PRODUCTION = ~_.indexOf(process.argv, "production");

module.exports = function () {
    var common = {
        entry: [
            './app/index.js',
            'babel-polyfill'
        ],
        output: {
            path: __dirname + "/app/dist",
            publicPath: "/dist/",
            filename: 'bundle.js'
        },
        devtool: "source-map",
        module: {
            rules: [
                {
                    test: /\.less/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: ['css-loader', 'less-loader']
                    })
                },
                {
                    test: /\.css$/,
                    loader: 'style-loader!css-loader'
                },
                {
                    test: /\.(jsx|js|html)$/,
                    exclude: /(node_modules)/,
                    loader: 'babel-loader',
                },
                {
                    test: /bootstrap\/dist\/js\/umd\//,
                    loader: 'imports?jQuery=jquery'
                },
                {
                    test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                    loader: 'url-loader?limit=100000'
                },
                {
                    test: /\.(jpe?g$|.gif$|.png$|.svg$|.woff$|.ttf$|.wav$|.mp3)$/,
                    loader: "file-loader?name=[path][name].[ext]"
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin({
                filename: 'bundle.css',
                allChunks: false
            }),
            new OptimizeCssAssetsPlugin(),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            })
        ],
        devServer: {
            contentBase: __dirname + "/app",
            historyApiFallback: true,
            hot: true,
            port: 7777,
        }

    };
    if (PRODUCTION) {
        common.plugins.push(
            new webpack.optimize.UglifyJsPlugin({
                debug: true,
                minimize: true,
                sourceMap: false,
                output: {
                    comments: false
                },
                compressor: {
                    warnings: false
                }
            })
        );

        common.plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production'),
                    APP_HOST: JSON.stringify('http://13.124.101.26')
                }
            })
        );
    } else {
        common.plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                    APP_HOST: JSON.stringify('http://api.mclaren.com')
                }
            })
        );
    }
    return common;
};
